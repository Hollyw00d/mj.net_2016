<?php

//Begin Really Simple SSL JetPack fix
define( "JETPACK_SIGNATURE__HTTPS_PORT", 80 );
//END Really Simple SSL
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/home/mattpjennings/mattjennings.net/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager //Added by WP-Cache Manager
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/* Database info */

// Local development ---> localhost
if ( $_SERVER['SERVER_NAME'] == "localhost" || preg_match('/^192\.168/', $_SERVER['SERVER_NAME']) > 0 )
{
  define('DB_NAME', 'mjnet_local_2016');
  define('DB_USER', 'root');
  define('DB_PASSWORD', 'root');
  define('DB_HOST', 'localhost');
}
// Production environment ---> Live site
else
{
  define('DB_NAME', 'mjnet2016database');
  define('DB_USER', 'mjnet2016user');
  define('DB_PASSWORD', 'B9CtNM$*L&EgCACEr2cJhuJLR7int2');
  define('DB_HOST', 'mj-net-2016.mattjennings.net');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pTn}@2Y:sx-; ;n.>UrQ*o5]4_=O&S%t91$Xht]4HfMU>~[sB%|Kaf|xeG!?He2y');
define('SECURE_AUTH_KEY',  'p^{+[4+uu]-<(Z|[XD=`nykX^wCX?-/p@$=@0hi}|xDz%3X:_Fe|?=8UP6iyq|yP');
define('LOGGED_IN_KEY',    '4z-tHa,e;q[e5{;PrS?-q!N&&gu+x[,_>:Y7IZ3uM+Aur||xr3p3wD%n%}gPAGX+');
define('NONCE_KEY',        '`>h]zFTKuQ:^3ewT;x1 P+p hNrw[q0j8xk8(?3|+.%R2x(Hs.ds`zXN6?zuP`lF');
define('AUTH_SALT',        '|!%Yr|S_M/I%-)jw1$+9mtz;<%!vnuB.w&Dbd^Y9T`.@|c>|dy2JigE_Z`eQutRB');
define('SECURE_AUTH_SALT', 'R}ooLcc/]U7_:t@)4)EmMt<ynS^xjL|J+)2b%FGz&0??0qjZ||uB_)14|x|j,e`M');
define('LOGGED_IN_SALT',   '~++;E(HH;F}.?@5g]Qj>|Jdg=8KEQ;VKDJ42J-F+0y^ N@;>2`KdY22#z6?gl6|3');
define('NONCE_SALT',       'D@%OjNDf1[sV]J~RdMh [nW3ygqM{8~G k%w%`T/*E>:>JB6u~ZI=xW|+T4?d[pG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_zwmgx2_';

/**
 * Limits total Post Revisions saved per Post/Page.
 * Change or comment this line out if you would like to increase or remove the limit.
 */
define('WP_POST_REVISIONS',  10);

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


// gulp
const gulp = require('gulp');

// JS gulp modules
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const babel = require('gulp-babel');

// CSS and Sass gulp modules
const cleanCSS = require('gulp-clean-css');
const sass = require('gulp-sass');

// Theme path modules
const mjNext2012ThemePath = 'wp-content/themes/MJ-net-2012';

gulp.task('js', () => {
    return gulp.src([
        `${mjNext2012ThemePath}/js/local.js`,
        `${mjNext2012ThemePath}/js/wp-admin.js`
    ])
        .pipe( rename({ suffix: '.min' }) )
        .pipe( babel() )
        .pipe( uglify() )
        .pipe(gulp.dest(`${mjNext2012ThemePath}/js`));
});

gulp.task('watchjs', () => {
  return gulp.watch([
    `${mjNext2012ThemePath}/js/local.js`,
    `${mjNext2012ThemePath}/js/wp-admin.js`
  ], ['js'])
});

gulp.task('sass', () => {
  return gulp.src(
    `${mjNext2012ThemePath}/**.scss`
  )
    .pipe(sass())
    .pipe(cleanCSS())
    .pipe(gulp.dest(mjNext2012ThemePath));
});

gulp.task('watchsass', () => {
  return gulp.watch(`${mjNext2012ThemePath}/**.scss`, ['sass']);
});

gulp.task('default', () => {
  return gulp.start(['watchjs','watchsass']);
});
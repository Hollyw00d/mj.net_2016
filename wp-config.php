<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

// Don't concatenate scripts
define('CONCATENATE_SCRIPTS', false );



/** Enable W3 Total Cache */

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */




// local development
if ( $_SERVER['SERVER_NAME'] == 'local.mattjennings.net' )  {
// ** MySQL settings ** //
    /** The name of the database for WordPress */
    define('DB_NAME', 'mattjennings_net_2');

    /** MySQL database username */
    define('DB_USER', 'root');

    /** MySQL database password */
    define('DB_PASSWORD', 'root');

    /** MySQL hostname */
    define('DB_HOST', 'localhost');

	define( 'WP_SITEURL', 'https://local.mattjennings.net/');
	define( 'WP_HOME', 'https://local.mattjennings.net/');
}
// staging
else if ( $_SERVER['SERVER_NAME'] == 'www.staging.mattjennings.net' || $_SERVER['SERVER_NAME'] == 'staging.mattjennings.net' ) {
// ** MySQL settings ** //
    /** The name of the database for WordPress */
    define('DB_NAME', 'staging_mattjennings_net');

    /** MySQL database username */
    define('DB_USER', 'stagingmattjenni');

    /** MySQL database password */
    define('DB_PASSWORD', '*9MuiEXP');

    /** MySQL hostname */
    define('DB_HOST', 'mysql.staging.mattjennings.net');

    define( 'WP_SITEURL', 'https://staging.mattjennings.net');
    define( 'WP_HOME', 'https://staging.mattjennings.net');
}
// production
else {
    // ** MySQL settings ** //
    /** The name of the database for WordPress */
    define('DB_NAME', 'mattjennings_net_2');

    /** MySQL database username */
    define('DB_USER', 'mattjenningsnet2');

    /** MySQL database password */
    define('DB_PASSWORD', 'KB4cngy!');

    /** MySQL hostname */
    define('DB_HOST', 'mysql.mattjennings.net');
}

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('DISALLOW_FILE_EDIT', true);

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HA->4!ugVL=^6;o)}ol_>E&$P#5~ekCT!0z9DZjo,Sc7G)4ddWq;;ohbQ;Ni4jb#');
define('SECURE_AUTH_KEY',  'vBeFuO*dOGvZ;m1n*zMrl,0_dY!f#@H+VJ>R-6U)>HFew]1*k)Aed|F}>8#6?H;7');
define('LOGGED_IN_KEY',    '$<::QR7(dX8QTcd?q-m930-qG5_yo/,zp[^1|=CD]%^wZVAJu&W<42gz^6ZqB*dP');
define('NONCE_KEY',        'LRbYm6RGJ.TDk/.dZc/>P:@0i{(/[dD<$(58!C]e40DX$F]aDQ*0Pde]`<WESq(M');
define('AUTH_SALT',        'I{kO.jV:tLcL4]t.4>4U?61}D(!BBn@|C:@zY7|sPCm>k*1-QC]08h|WA8sx@|7F');
define('SECURE_AUTH_SALT', '5%-{efA-<J40Y[GBP?!@kHg9]P7:y,g|nU*r1z!|R~h.Ti&0>Qe8-T qog#&,Ei-');
define('LOGGED_IN_SALT',   'ya+z{gTtV-eOPa5cS*yZm_2F;|Zu8Ct<uuJoKOl;6-l%F+|L7sSI?B.[y09!p7y}');
define('NONCE_SALT',       '[F0.h,Vxc),J~)Ka2.dR8!NFgio<~R8i*5#DQmEu}|UY_M59&L4K_h<fsEzS3C!t');


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_zwmgx2_';

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

